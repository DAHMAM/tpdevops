pipeline {
    agent any
    
    environment {
        ANSIBLE_HOME = "${WORKSPACE}/ansible"
        SSH_KEY = credentials('vmazure')
    }
    
    stages {
        stage('Build') {
            agent {
                docker {
                    image 'maven:3.9.6-eclipse-temurin-22-alpine'
                    args '-u root -v $HOME/.m2:/root/.m2'
                    reuseNode true
                }
            }
            steps {
                sh 'mvn package -DskipTests'
                archiveArtifacts artifacts: 'target/*.jar', followSymlinks: false
            }
        }

        stage('Unit Tests') {
            agent {
                docker {
                    image 'maven:3.9.6-eclipse-temurin-22-alpine'
                    args '-u root -v $HOME/.m2:/root/.m2'
                    reuseNode true
                }
            }
            steps {
                sh 'mvn surefire:test'
                junit allowEmptyResults: true, testResults: 'target/surefire-reports/*.xml'
            }
        }

        stage('Publish Docker Image') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'dockerhub', usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD')]) {
                        sh 'docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD'
                        sh 'docker build -t az990/tpdevops .'
                        sh 'docker push az990/tpdevops'
                        sh 'docker logout'
                    }
                }
            }
        }

        stage('Deploy') {
            agent {
                docker {
                    image 'az990/ansible-minikube:latest' // Utilise une image Docker contenant Ansible
                    args '-u root'
                }
            }
            
            steps {
                script {
                    // Utiliser une seule commande sh pour persister le changement de répertoire
                    sh '''
                        mkdir -p ~/.ssh
                        ssh-keyscan -H 20.94.232.140 >> ~/.ssh/known_hosts
                    '''
                    sh '''
                        ssh -i $SSH_KEY houssem3@20.94.232.140 << 'EOF'
                        cd /home/houssem3/tpdevops/ansible
                        ansible-playbook deploy.yml -i inventory --extra-vars "image=az990/tpdevops:latest"
                        EOF || echo "hello world"
                    '''
             }
         
    }
}
}}

