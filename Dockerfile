FROM openjdk:22-jdk
RUN groupadd --system spring && useradd --system --gid spring spring
COPY /target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
